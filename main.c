#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>


typedef struct estr {
    char ch;
    struct estr *prox;
} NO;

typedef struct {
    NO*inicio;
} LISTA_DIN;

void imprime(LISTA_DIN l){
    NO* a = l.inicio;
    while(a){
        printf("%c", a->ch);
        a = a->prox;
    }
}

NO* ultimoElemLista(LISTA_DIN l) {
    NO* p = l.inicio;
    if(p)
    while(p->prox) p = p->prox;
    return(p);
}

void anexarElemLista(char ch, LISTA_DIN *l) {
    NO* novo;
    NO* ant;
    ant = ultimoElemLista(*l);
    novo = (NO *) malloc(sizeof(NO));
    novo->ch = ch;
    novo->prox = NULL;
    if(!ant) l->inicio = novo;
    else ant->prox = novo;
}

NO* decodificar(char* entrada) {
    LISTA_DIN* l = (LISTA_DIN*)malloc(sizeof(LISTA_DIN));
    LISTA_DIN nova_lista;
    nova_lista.inicio = NULL;
    l->inicio = (NO*)malloc(sizeof(NO));
    NO* p = NULL;
    p = l->inicio;
    if(strlen(entrada) == 1) {
        NO* novo = (NO*)malloc(sizeof(NO));
        novo->ch = entrada[0];
        novo->prox = NULL;
        nova_lista.inicio = novo;
        return novo;
    }

    int i = 0;
    while(entrada[i]!='\0'){
        if((entrada[i]<=57)&&(entrada[i]>=48 && entrada[i + 1] != '\0')){
            int n = entrada[i] - 48;
            i++;
            for(int x = 0; x < n; x++){
                p->ch = entrada[i];
                anexarElemLista(p->ch, &nova_lista);
                p->prox = (NO*)malloc(sizeof(NO));
                p = p->prox;
            }
        }else{
            p->ch = entrada[i];
            anexarElemLista(p->ch, &nova_lista);
            p->prox = (NO*)malloc(sizeof(NO));
            p = p->prox;
        }i++;
    }p->prox = NULL;
    
    return nova_lista.inicio;
}


int main() {
    decodificar("oi9a3");
}